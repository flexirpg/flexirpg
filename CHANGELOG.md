# 2.1.0

- Added a ruler tool for making simple measurements. Add points with
  left-click, discard the ruler with right click. GMs can change the
  scale, units and measurement method in the map properities.
- Updated icon etc. build for newer versions of python and inkscape.

# 2.0.2

- Tweak map rendering to possibly fix issues on MacOS.
- Drop dependency on Pillow.

# 2.0.1

- Fix bug where newly connected client would not get a fog layer.

# 2.0.0

- Added tile-based fog-layer, giving GMs tools to hide/reveal areas of
  the map.
- Improve rendering of grid line by using transparency.
- Fix bug with miniature heading/facing on Python 3.10 and later.

# 1.99.7

- Fix TypeError exceptions during startup on Python 3.10 and later.

# 1.99.6

- Disable adding or importing game tree nodes while the tree is
  locked.
- Ignore dragged game tree nodes if they were dragged outside of the
  tree. This should address the issue where nodes ended up in
  unexpected places.

# 1.99.5

- Add server option (default_role) to set the default role for players
  joining a room. This option defaults to "Player" (instead of the
  previous fixed default of "Lurker").

# 1.99.4

- Add "Create Miniature" button to the miniature library that allows
  the creation of basic miniatures using a fixed style with tweakable
  colours, size and text.

# 1.99.3

- Scale larger miniatures on toolbar button (instead of showing just a
  corner).
- Remember the last used folder when saving/opening files.
- Select the most recently added miniature when adding miniatures in
  the miniature library.

# 1.99.2

- Add buttons to the miniature library for reordering the minis.
- Fix various issues with background and miniature images not loading.
- Fix miniatures added from the map not showing in library.

# 1.99.1

- Fix loading of map files using legacy UUID-based images.
- Fix miniature preview on MacOS.
- Minor usabilility improvements with the miniature library.

# 1.99.0

- Add a basic interface for managing miniatures.
- Fix non-square miniatures not being shown.
- Fix the server incorrecly handling out-dated clients.
- An incompatible protocol change to use content-based IDs (SHA-256
  hashes) to identify images. This is a backward-incompatible change
  and 1.99 servers only support 1.99 or later clients. Old map files
  and miniature libraries can be loaded and will be converted when
  saved.

# 1.98.4

- Fix dice roll buttons after typing count or modifer on MacOS.
- Fix layout of send node to player dialog on MacOS.
- Fix miniature and tree icon selection on MacOS.
- Don't import wx library when starting server.

# 1.98.3

- Mouse pointer changes on map depending on selected mode.
- Fix GM whispers.
- Fix adding miniatures to library.
- Fix nodes not updating after clicking a dice roll if the node was
  scrolled off screen.
- Possibly fix issue with dice toolbar count/modifier values on MacOS.

# 1.98.2

- Can use variables from active node when typing dice rolls.
- Minor usability tweak to dice rolls from the toolbar.
- Fix clicking on dice rolls regression.
- Fix sending whispers regression.
- Fix creating whisper tab regression.

# 1.98.1

- Fix saving of preferences and settings when the installed version of
  sqlite3 is older than 3.24 (affects Ubuntu 18.04 and Linux Mint 19.3).
- Fix role change regression.

# 1.98.0

- Make a node "Active" to use its action for player status. This
  should be useful for showing current HP, AC, status etc.
- Player roles shown with icons.
- Typing status shown with ... icon (requires a 1.98.0 or later
  server) rather than changing the status text.
- Draw circles and cones (quarter circles) on the map.
- Improved usability and style of various dialogs.
  - Server/room browser.
  - Map text properties.
  - Settings (but not all settings are in the new dialog yet,
    particularly various font settings).
- Removed some unused or less useful functionality.
  - Persistent rooms
  - Lots of junk chat commands.
  - Less useful chat and logging settings.
  - Game tree is always in ~/.flexirpg/tree.xml (or the Windows
    equivalent).
- Refactoring of the settings backend which will help adding new
  functionality in the future, but everything will need to be
  configured again as the old settings file is not imported.

# 1.97.5

- Fix snapping of miniatures on 3/4 of the map.

# 1.97.4

- Map save button saves again.
- Load some default minis if miniatures library is empty.

# 1.97.3

- Only require python 3.6.
- Improved map and chat toolbars.
- Only prompt for password for role changes if a password is set
  (requires updated server).

# 1.97.2

- Can lock game tree to prevent node delete/move.
- Move Delete All Objects from map context menu to Map -> Clear Map in
  main menu.
- Zoom usability improvements:
  - More intuitive.
  - Scroll wheel zooms.
  - New icons.

# 1.97.1

- Fix whispers.

# 1.97.0

- Publish to PyPI.
- Port to python 3.
- wxPython 4.1 compatibility.
- Resize player list columns when players are added/removed.

# 1.96.3

* Fix performance regression with newer versions of PLY.
* Fix snapping of newly added miniatures to the whiteboard.

# 1.96.2

* Allow node expressions to set magic variables (__name, __enabled,
  __icon, etc.) directly.
* Automatically update icon, name, and enable state if their magic
  variables are set by expressions.
* Replace the "__reset" variable with the more helpfully named
  "__roll".
* Deprecate the "Auto reset" feature of nodes and the __reset magic
  variable as it is no longer required (existing nodes will continue
  to work but this feature can no longer be enabled).
* Fix "Add Miniatures to Library...".
* Fix new miniatures added to map not showing correctly until they were moved.
* Fix old map background image showing after reconnecting.
* Fix "not" operator in dice expressions.

# 1.96.1

* Work with wxPython 4.0
* Support JPEG format images for miniatures and map backgrounds.

# 1.95.1

* Fixes a bug where map images may not load.

# 1.95.0

* Use a local PNG image for the map background (Map -> Properties,
  check Image, then press Select).
* Add miniatures to the library using local PNG images (Map -> Add
  Miniatures to Library...).
* Add miniatures to the library by right-clicking a miniature.
* Images (for map background and miniatures) are distributed by the
  server and no longer need to be hosted by a separate web server.

# 1.94.3

* Fix a map background regression in 1.94.2.
* Support wxPython 3.x.

# 1.94.2

* Fix a bug the map wouldn't load correctly for the DM when set via
  the map properties.

# 1.94.1

* Fix a bug where the server or client could hang if the client/server
  was unreachable or not responding.

# 1.94.0

* 1.94.0 servers will only accept connection from 1.94.0 or later
  clients.
* Improved server robustness when client are disconnected.
* Removed Game Tree -> Open since it was broken.  Use the tree
  import/export feature instead.
* Removed some less useful functionality from the server (remote
  administration and room voice/moderation).

# 1.93.1

* Fix a crash that occurs after deleting nodes with auto-reset
  enabled.
* Support non-ASCII characters in the game tree.

# 1.93.0

* Enable/disable tree nodes with the __enabled magic variable.
  Disabled nodes are grayed out and cannot be used.
* Change the node icon by assigning the icon's name to the __icon
  magic variable.
* New Action when reset, which can be triggered from the reset option
  on the right-click menu.  Resetting a node reenables it and also
  resets all its children.
* Reset "Auto Reset" enabled nodes by clicking on dice rolls in the
  chat window.  The __reset magic variable is set to the result of the
  last clicked roll.

# 1.92.3

* Chat window auto-scroll works better.

# 1.92.2

* Chat window won't automatically scroll when new text appears unless
  its already scrolled to the end.  This should make selecting or
  reading earlier text easier.
* Tabbed whisper setting works correctly if it has been disabled.
* Timestamps in the chat window are displayed using the current
  locale.

# 1.92.1

* If whisper tabs are enabled, a tab is opened if "whisper" is
  selected from the player list.
* convert-tree.py handles more trees.
* Copying text from the chat window now works.
* Fixed flexinode performance regression introduced in 1.92.0.

# 1.92.0

* New set of tree icons.  The icons are now selected from the node editor.
* All support for game tree nodes except for flexinodes has been
  removed.
* The server GUI has been removed and the server is now started with
 `flexirpg-server.py`.
* A `__parent_name` magic variable has been added.  It is similar to
  `__name` except the parent node is used.
* Map is now detachable by default on new installations.  For existing
  installs, edit `settings.xml` to set the direction of the 'Chat
  Window' to 'Center' and the 'Map Window' to 'Top'.
* The "Show Labels" map option is remembered.
* Memory usage is greatly reduced when the tree contains many
  thousands of nodes.
* Several bugs in the handling of dice are fixed.

# 1.91.1

* Line breaks are allowed in dice expressions.
* Deleting characters in the dice expression works correctly.

# 1.91.0

* Logical 'and', 'or', and 'not' operators in dice expressions.
* Larger (multiline) action box in flexinode design window.


# 1.90.1

* Middle mouse button may be used to scroll the map.
* Selecting miniatures next to lines is easier.
* No longer needs to be run for the first time from the installation
  directory.
* Other bug fixes and minor usability improvements.

# 1.90.0

* The map is now infinite.
* Map tabs are no more.
* Clients now see all map grid changes.

# 1.11.0

* Add a preview button to the flexinode design window.
* Fix random crashes and freezes if a miniature image could not be
  loaded.
* Fix crash when starting on Ubuntu 10.10 system (and other system
  with wxPython 2.8.11.0 or later).

# 1.10.0

* Even more flexible flexinodes and dice expressions.
** Dice expressions can be strings.  Strings are enclosed in double
   quotes (") and can contain dice rolls.
** Actions can change the node name by assigning to the special
   `__name` variable.
** Dice expressions beginning with a # will not be displayed (e.g.,
   "aa[# a = 1d20]zz" will assign to 'a' and be displayed as "aazz").
* A "New Flexinode" option has been added to the game tree right-click
  context menu.
* Dice expressions with variables will be displayed correctly.
* Activating a game tree node with the mouse will reset the keyboard
  focus to the chat window.
* Scroll ON/Scroll OFF button on the chat window toolbar has been
  removed.

# 1.8.0

* Non-backward compatible network protocol and map file format.
* Server bookmarks.  The last 5 servers successfully connected to will
  be remembered.
* Significant performance increases in the map, particularly the fog
  layer and whiteboard lines.
* Loading or saving the game tree will no longer change where it is
  automatically loaded or saved from.  Use the gametree properties to
  change the location.
* Problems with the room list occasionally not appearing after a
  connection are resolved.
* "Join Room" button on the server browser now works again.
* Left-clicking selected nodes in the game tree will allow the name to
  be edited.
* Mouse cursor no longer gets stuck as a 'X'.

# 1.7.9


* First release under the FlexiRPG name.
* Removed character tools with trademarked names (Star Wars, D&D 3rd
  and 3.5 edition).
* Better support for Unicode characters throughout.
* Improved latency (by up to 0.5 s) in the server's processing of
  received messages.

# 1.7.8-rc4-dv8 (OpenRPG)

* Improved whiteboard line drawing with better performance and less
  flicker.
* Fixed bug where incorrect lines or text could be deleted if multiple
  users had drawn on the whiteboard.
* Option to hide miniature labels.

# 1.7.8-rc4-dv7 (OpenRPG)

* Minor style/usability changes to flexinode design window.
* Improved look on the map toolbar (controls are no longer partially
  hidden etc.).
* Fixed bug causing map miniature synchronization problems (miniatures
  not moving, labels not updating).


# 1.7.8-rc4-dv6 (OpenRPG)

* Flexinode variables can be simple numeric expressions.  (e.g., Str =
  (strength - 10) / 2).
* Flexinode variable names can include underscores.
* The flexinode action's node name in the chat text is now bold.  The
  node name can also be suppressed by starting the action with a
  colon.

# 1.7.8-rc4-dv5 (OpenRPG)

* Dragging and dropping game tree nodes can move the node to before,
  after or below the target node by dropping the node on the top half,
  bottom half, or to the right of the target node's label.  Still no
  visual cues for where it will end up.
* Fetching of server lists from the meta server has been disabled.
  This server list functionality will eventually be replaced by
  client-side server "bookmarks".
* Preliminary flexinode:
** Flexinodes can be nested arbitrarily.
** Unlimited number of variables in each flexinode.
** Variables references are resolved in action text dice rolls.
** Variable expressions are limited to integers.

# 1.7.8-rc4-dv4 (OpenRPG)

* Create all user file subdirectories even if some of them exist
  already.

# 1.7.8-rc4-dv3 (OpenRPG)

* When a game tree fails to load, say why and allow a different one to
  be loaded.
* Set the font used for miniature labels.
* [linux] Fix mouse cursor always being an arrow.

# 1.7.8-rc4-dv2 (OpenRPG)

* Fix exception when starting on Windows.

# 1.7.8-rc4-dv1 (OpenRPG)

* User files (game tree etc.) are now in %APPDATA%\OpenRPG (on
  Windows) or $HOME/.openrpg/ (on Linux).
* Simpler openrpg.py to start OpenRPG.
* Fixed loading of d20 nodes.
* Command line server works again.
* Removed splash screen.
* Corrected skills in d20 node template.
