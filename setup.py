import setuptools
import setuptools.command.build_py
import subprocess

with open("README.md", "r") as fh:
    long_description = fh.read()

class BuildPyCommand(setuptools.command.build_py.build_py):
    """Build icons."""

    def run(self):
        subprocess.run(["make"], check=True)
        setuptools.command.build_py.build_py.run(self)

setuptools.setup(
    name="flexirpg",
    use_scm_version=True,
    author="David Vrabel",
    author_email="dvrabel@cantab.net",
    description="Flexible RPG virtual tabletop application",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/flexirpg/flexirpg",
    setup_requires=["setuptools_scm"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)",
        "Topic :: Games/Entertainment :: Board Games",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[
        "cmd2",
        "importlib_metadata",
        "ply>=3.11",
        "setuptools",
        "wxPython>=4",
    ],
    package_data={
        "orpg": [
            "images/*.ico",
            "images/*.png",
            "images/*.xml",
            "templates/*.html",
            "templates/*.xml"
        ],
    },
    entry_points={
        "console_scripts": [
            "flexirpg=orpg.main:run_client",
            "flexirpg-server=orpg.server_cli:run_server",
        ],
    },
    cmdclass={
        "build_py": BuildPyCommand,
    },
)
