# Copyright 2020 David Vrabel
# Copyright (C) 2000-2001 The OpenRPG Project
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from threading import RLock
import weakref

import orpg.lib.xmlutil as xmlutil

class map_element_msg_base:
#  This is a base class

    def __init__(self, tagname, parent=None):
        self.tagname = tagname
        #  This is a dictionary that holds (value,changed) 2-tuples, indexed by attribute
        #  Avoid manipulating these values directly.  Instead, use the provided accessor methods.
        #  If there is not one that suits your needs, please add one to this class and
        #  use it instead.
        self._props = {}

        # Take a weakref on the parent so we don't need to explicitly
        # delete children.
        if parent is not None:
            self.parent = weakref.ref(parent)
        else:
            self.parent = None
        self.children = {}
        self._changed = False

        # Use the parent's lock, or create a new one.
        if parent is not None:
            self.p_lock = parent.p_lock
        else:
            self.p_lock = RLock()

    def clear(self):
        self.p_lock.acquire()

        for child in list(self.children.keys()):
            self.children[child].clear()
            self.children[child] = None
            del self.children[child]
        self._props.clear()
        self.changed = True

        self.p_lock.release()

    @property
    def changed(self):
        return self._changed

    @changed.setter
    def changed(self, ch):
        self._changed = ch
        if ch and self.parent is not None:
            self.parent().changed = True

    #########################################
    #  Accessor functions begin

    #  Access single property

    def init_prop(self,prop,value):  # set's the changed flag to False and assigns
        self.p_lock.acquire()
        self._props[prop] = (value, False)
        self.p_lock.release()

    def set_prop(self,prop,value):  # set's the changed flag to True and assigns
        self.p_lock.acquire()
        self._props[prop] = (value, True)
        self.changed = True
        self.p_lock.release()

    def get_prop(self,prop):  # returns None if prop not found
        self.p_lock.acquire()
        if prop in list(self._props.keys()):
            (p,c) = self._props[prop]
            self.p_lock.release()
            return p
        else:
            self.p_lock.release()
            return None

    def is_prop_changed(self,prop):  # returns None if prop not found
        self.p_lock_acquire()
        if prop in list(self._props.keys()):
            (p,c) = self._props[prop]
            self.p_lock.release()
            return c
        else:
            self.p_lock.release()
            return None

    def get_child(self,key):         # returns None if key not found in children list
        self.p_lock_acquire()
        if key in self.children:
            self.p_lock.release()
            return self.children[key]
        else:
            self.p_lock.release()
            return None

    #  Access multiple properties

    def init_props(self,props):               # same as init_prop(), but takes dictionary of props
        self.p_lock.acquire()
        for k in list(props.keys()):
            self._props[k] = (props[k],0)
        self.p_lock.release()

    def set_props(self,props):                # same as set_prop(), but takes dictionary of props
        self.p_lock.acquire()
        for k in list(props.keys()):
            self._props[k] = (props[k],1)
        self.p_lock.release()

    def get_all_props(self):                  # returns dictionary of all properties, regardless of change
        self.p_lock.acquire()

        result = {}
        for k in list(self._props.keys()):
            (p,c) = self._props[k]
            result[k] = p

        self.p_lock.release()
        return result

    def get_changed_props(self):              # returns dictionary of all properties that have been changed
        self.p_lock.acquire()
        result = {}
        for k in list(self._props.keys()):
            (p,c) = self._props[k]
            if c:
                result[k] = p
        self.p_lock.release()
        return result

    def get_children(self):                 # returns dictionary of children
        return self.children


    #  Accessor functions end
    #########################################

    #########################################
    #  XML emitters begin
    def get_all_xml(self,action="new",output_action = 0):    # outputs a tag with all attributes it contains
        self.p_lock.acquire()
        xml_str = "<" + self.tagname

        if action and output_action:
            xml_str += " action='" + action + "'"

        for k in list(self._props.keys()):
            (p,c) = self._props[k]
            if k != "action" or not action:
                xml_str += " " + k + "='" + p + "'"

        if self.children:
            xml_str += ">"

            for child in list(self.children.keys()):
                xml_str += self.children[child].get_all_xml(action)

            xml_str += "</" + self.tagname + ">"

        else:
            xml_str += "/>"


        self.p_lock.release()

        return xml_str

    def get_changed_xml(self,action="update",output_action = 0):    # outputs a tag with all changed attributes
        self.p_lock.acquire()
        xml_str = "<" + self.tagname

        if action and output_action:
            xml_str += " action='" + action + "'"

        for k in list(self._props.keys()):
            (val, changed) = self._props[k]
            if changed or k == "id": # Always include the "id" attribute.
                xml_str += f" {k}='{val}'"
                self._props[k] = (val, False)

        if self.children:
            xml_str += ">"
            for child in self.children.values():
                if child.changed:
                    xml_str += child.get_changed_xml(action)

            xml_str += "</" + self.tagname + ">"

        else:
            xml_str += "/>"

        self.changed = False
        self.p_lock.release()
        return xml_str


    # convenience method to use if only this tag is modified
    #   outputs a <map/> element containing only the changes to this tag
    def standalone_update_text(self,update_id_string):
        buffer = "<map id='" + update_id_string + "'>"
        buffer += self.get_changed_xml("update")
        buffer += "<map/>"
        return buffer

    # XML emitters end
    #########################################

    #########################################
    #  XML importers begin

    def _from_dom(self,xml_dom,prop_func):
        self.p_lock.acquire()
        if xml_dom.tagName == self.tagname:
            for a in range(xml_dom.attributes.length):
                attr = xml_dom.attributes.item(a)
                self.init_prop(attr.nodeName, attr.nodeValue)
        else:
            self.p_lock.release()
            raise Exception("Error attempting to modify a " + self.tagname + " from a non-<" + self.tagname + "/> element")
        self.p_lock.release()

    def init_from_dom(self,xml_dom):

    #  xml_dom must be pointing to an empty tag.  Override in a derived class for <map/> and other similar tags.
        self._from_dom(xml_dom,self.init_prop)

    def set_from_dom(self,xml_dom):

    #  xml_dom must be pointing to an empty tag.  Override in a derived class for <map/> and other similar tags
        self._from_dom(xml_dom,self.set_prop)

    def init_from_xml(self,xml):
        xml_dom = xmlutil.parseXml(xml)
        node_list = xml_dom.getElementsByTagName(self.tagname)

        if len(node_list) < 1:
            print("Warning: no <" + self.tagname + "/> elements found in DOM.")
        else:
            while len(node_list):
                self.init_from_dom(node_list.pop())

        if xml_dom:
            xml_dom.unlink()

    def set_from_xml(self,xml):
        xml_dom = xmlutil.parseXml(xml)
        node_list = xml_dom.getElementsByTagName(self.tagname)

        if len(node_list) < 1:
            print("Warning: no <" + self.tagname + "/> elements found in DOM.")
        else:
            while len(node_list):
                self.set_from_dom(node_list.pop())


        if xml_dom:
            xml_dom.unlink()


    # XML importers end
    #########################################
