# Copyright (C) 2000-2001 The OpenRPG Project
#
#    openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from math import *
from threading import Lock
import time

import wx


class cmpPoint(wx.Point2D):
    def __init__(self,*_args,**_kwargs):
        wx.Point.__init__(self,*_args,**_kwargs)

    def __cmp__(self,other):
        try:
            if self.x < other.x:
                return -1
            elif self.x > other.x:
                return 1
            elif self.y < other.y:
                return -1
            elif self.y > other.y:
                return 1
            else:
                return 0

        except:
            return -2





class cmpColour(wx.Colour):
    def __init__(self,*_args,**_kwargs):
        wx.Colour.__init__(self,*_args,**_kwargs)

    def __cmp__(self,other):
        try:
            (r,g,b) = self.Get()
            my_value = b*256*256 + g*256 + r
            (r,g,b) = other.Get()
            other_value = b*256*256 + g*256 + r

            if my_value < other_value:
                return -1
            elif my_value > other_value:
                return 1
            else:  # they're equal
                return 0
        except:
            return -2



class protectable_attributes:

    def __init__(self):
        self._set("_protected_attr",[])

    def _set(self,name,value):
        self.__dict__[name] = value

    def __setattr__(self,name,value):
        if name in self._protected_attr:
            full_name = "_protect_" + name
            if hasattr(self,full_name):
                (p,c) = getattr(self,full_name)
                if p != value:
                    self._set(full_name,(value,1))
            else:
                self._set(full_name,(value,1))
        else:
            self._set(name,value)

    def __getattr__(self,name):
        if name in self._protected_attr:
            (p,c) = self.__dict__["_protect_" + name]
            return p
        else:
            raise AttributeError

    def _clean_attr(self,name):
        if name in self._protected_attr:
            (p,c) = self.__dict__["_protect_" + name]
            self._set("_protect_" + name,(p,0))


    def _dirty_attr(self,name):
        if name in self._protected_attr:
            (p,c) = self.__dict__["_protect_" + name]
            self._set("_protect_" + name,(p,1))


    def _changed_attr(self):
        changed = {}
        for name in self._protected_attr:
            (p,c) = self.__dict__["_protect_" + name]
            if c:
                changed[name] = p
        return changed

    def _clean_all_attr(self):
        for name in self._protected_attr:
            (p,c) = self.__dict__["_protect_" + name]
            self._set("_protect_" + name,(p,0))

    def _dirty_all_attr(self):
        for name in self._protected_attr:
            (p,c) = self.__dict__["_protect_" + name]
            self._set("_protect_" + name,(p,1))


##-----------------------------
## base class for layer objects
##-----------------------------

class layer_base:

    def __init__(self):
        self.lock = Lock()

    def layerDraw(self,dc,scale='',topleft='',size=''):
        pass

    def layerToXML(self, action):
        pass

    def layerTakeDOM(self, xml_dom):
        pass

    def hit_test(self,pos):
        pass

    def send_updates(self):
        xml = self.layerToXML()
        if xml != "":
            self.canvas.frame.session.send("<map>" + xml + "</map>")
