#
# FlexiRPG Makefile
#
# Copyright 2020 David Vrabel
#

all: icons ply-tables

icons:
	make -C orpg/images

ply-tables:
	python3 -m orpg.dieroller.parser
