# FlexiRPG

## Introduction

FlexiRPG is a virtual tabletop for pen and paper role playing games.

It aims to be rule system agnostic by providing flexible tools and
features.

FlexiRPG was originally derived from [OpenRPG](http://www.rpgobjects.com/index.php?c=orpg).

## Requirements

* [Python](http://www.python.org/) (3.7 or later).
* [wxPython](http://www.wxpython.org/) 4.0.6 or later.
* [PLY](http://www.dabeaz.com/ply/) 3.11 or later.

## Installing dependencies.

### pip

    sudo apt-get install python3-pip

### PLY

    pip3 install --user -U ply

### wxPython

Install a pre-built package for your OS or Linux distribution.

If there is no suitable pre-built package it will need to be built
from source using `pip install`.

1. Remove `libwebkit2gtk-3.0-dev` (as the wxWidgets will build with
   this but wxPython expects a built with WebKit 1):

       sudo apt-get remove libwebkit2gtk-3.0-dev

2. Install dependencies:

       sudo apt-get install g++ libjpeg-dev libgtk-3-dev libgl1-mesa-dev \
           libglu1-mesa-dev libgstreamer1.0-dev \
           libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev \
           python-dev libwebkitgtk-3.0-dev`

3. Install with pip:

       pip3 install --user -U wxpython

4- Troubleshooting

If you get an error on while trying to fire up flexi.rpg
`ImportError: libpng12.so.0: cannot open shared object file: No such file or directory`

Follow instructions at: 

[text](https://www.linuxuprising.com/2018/05/fix-libpng12-0-missing-in-ubuntu-1804.html)

to install libpeng12-0 on your system

## Upgrading from Releases before 1.96.2

Nodes using the auto-reset feature and __reset magic variable should
be updated to use the new, simpler method by:

1. Replacing expression that update `__name` to set the `__name`
   variable directly. (e.g., `update_hp = __name = "HP [hp] / [total_hp]"`
   should be `__name = "HP [hp] / [total_hp]`).

2. Removing any dice rolls used to trigger name, or icon updates.

3. Using `__roll` in expressions instead of reading `__reset` in a
   reset action (e.g., a damage node could have a
   `__name = "Take [__roll] damage"` expression).

4. Clearing the "Auto reset" checkbox and removing any reset action.

## Upgrading from Releases before 1.95.0

Maps saved in versions prior to 1.95.0 are not compatible and may not
load correctly.

If you manually added miniatures to the `~/.flexirpg/miniatures.xml`
configuration file, these will need to be re-added to the library
using the new menu option (Map -> Add Miniature to Library...).

## Development

In addition to the requirements listed above you will need:

- Inkscape (https://www.inkscape.org/)
- make

### Development Installation

1. Setup a new virtual environment.

   For example:

       python3 -m venv ../flexi-dev
       . ../flexi-dev/bin/activate

2. Do a development install.

       pip install -e .

3. Build all the PNG images from the SVG source.

       make
