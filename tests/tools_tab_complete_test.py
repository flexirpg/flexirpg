# Tab completion tests.
#
# Copyright (C) 2019 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from orpg.tools.tab_complete import tab_complete

def test_tools_tab_complete():
    assert tab_complete("abc", []) == None
    assert tab_complete("abc", ["abc"]) == "abc"
    assert tab_complete("def", ["abc"]) == None
    assert tab_complete("abc", ["abcd", "abcde"]) == "abcd"
    assert tab_complete("abc", ["abcd1", "abcd2"]) == "abcd"
