# Settings tests.
#
# Copyright (C) 2020 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import unittest.mock as mock
import os
import sqlite3

import pytest
import wx

from orpg.tools.settings import SettingCollection

def add_to_db(config_path, name, value_string):
    conn = sqlite3.connect(config_path)
    c = conn.cursor()
    c.execute("INSERT INTO settings (name, value) VALUES (?, ?)",
              (name, value_string))
    conn.commit()
    conn.close()

def read_db(config_path):
    conn = sqlite3.connect(config_path)
    c = conn.cursor()
    # Have only a single "settings" table?
    c.execute("SELECT name FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%'")
    assert c.fetchone() == ("settings",)
    assert c.fetchone() == None

    c.execute("SELECT name, value FROM settings ORDER BY name")
    results = c.fetchall()

    conn.close()
    return results

def test_settings_settingcollection_string(config_path):
    settings = SettingCollection(config_path)

    add_to_db(config_path, "s1", "S1")

    settings.define("s1", "d1")
    settings.define("s2", "d2")

    s1 = settings.lookup("s1")
    assert s1.name == "s1"
    assert s1.default == "d1"
    assert s1.value == "S1"

    s2 = settings.lookup("s2")
    assert s2.name == "s2"
    assert s2.default == "d2"
    assert s2.value == "d2"

    # Set value and insert into DB.
    s2.value = "S2"
    assert s2.default == "d2"
    assert s2.value == "S2"

    raw_settings = read_db(config_path)
    assert raw_settings == [("s1", "S1"), ("s2", "S2")]

    # Set value and update entry in DB.
    s2.value = "S3"
    assert s2.default == "d2"
    assert s2.value == "S3"

    raw_settings = read_db(config_path)
    assert raw_settings == [("s1", "S1"), ("s2", "S3")]

def test_settings_settingcollection_int(config_path):
    settings = SettingCollection(config_path)

    add_to_db(config_path, "s1", "111")

    settings.define("s1", 11)
    settings.define("s2", 22)

    s1 = settings.lookup("s1")
    assert s1.name == "s1"
    assert s1.default == 11
    assert s1.value == 111

    s2 = settings.lookup("s2")
    assert s2.name == "s2"
    assert s2.default == 22
    assert s2.value == 22

    s2.value = 222
    assert s2.default == 22
    assert s2.value == 222

    raw_settings = read_db(config_path)
    assert raw_settings == [("s1", "111"), ("s2", "222")]

def test_settings_settingcollection_bool(config_path):
    settings = SettingCollection(config_path)

    add_to_db(config_path, "s1", "true")

    settings.define("s1", False)
    settings.define("s2", False)

    s1 = settings.lookup("s1")
    assert s1.name == "s1"
    assert s1.default == False
    assert s1.value == True

    s2 = settings.lookup("s2")
    assert s2.name == "s2"
    assert s2.default == False
    assert s2.value == False

    s2.value = True
    assert s2.default == False
    assert s2.value == True

    raw_settings = read_db(config_path)
    assert raw_settings == [("s1", "true"), ("s2", "true")]

def test_settings_settingcollection_color(config_path):
    settings = SettingCollection(config_path)

    add_to_db(config_path, "s1", "rgba(12,34,56,78)")
    c1 = wx.Colour(12, 34, 56, 78)
    c2 = wx.Colour(23, 45, 67, 89)
    c3 = wx.Colour(34, 56, 78, 90)

    settings.define("s1", c1)
    settings.define("s2", c2)

    s1 = settings.lookup("s1")
    assert s1.name == "s1"
    assert s1.default == c1
    assert s1.value == c1

    s2 = settings.lookup("s2")
    assert s2.name == "s2"
    assert s2.default == c2
    assert s2.value == c2

    s2.value = c3
    assert s2.default == c2
    assert s2.value == c3

    raw_settings = read_db(config_path)
    assert raw_settings == [("s1", "rgba(12,34,56,78)"), ("s2", "rgba(34,56,78,90)")]

def test_settings_settingcollection_define_duplicate(config_path):
    settings = SettingCollection(config_path)
    settings.define("s", 0)
    with pytest.raises(KeyError):
        settings.define("s", 0)

def test_setting_settingcollection_wrong_type(config_path):
    settings = SettingCollection(config_path)
    settings.define("s", 0)
    s = settings.lookup("s")
    with pytest.raises(TypeError):
        s.value = "not an int"

def test_setting_settingcollection_unsupported_type(config_path):
    class UnsupportedType:
        pass

    settings = SettingCollection(config_path)
    with pytest.raises(TypeError):
        settings.define("s", UnsupportedType())

def test_setting_setting_watch(config_path):
    settings = SettingCollection(config_path)
    s1 = settings.define("s1", 1)

    c1 = mock.Mock()
    c2 = mock.Mock()

    s1.watch(c1)
    s1.watch(c2)

    s1.value = 2

    c1.assert_called_with(s1)
    c2.assert_called_with(s1)

def test_setting_setting_watch_unchanged(config_path):
    settings = SettingCollection(config_path)
    s1 = settings.define("s1", 1)

    c1 = mock.Mock()
    c2 = mock.Mock()

    s1.watch(c1)
    s1.watch(c2)

    s1.value = 1

    c1.assert_not_called()
    c2.assert_not_called()
