# fog_msg tests
#
# Copyright (C) 2020 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import xml.dom.minidom

from orpg.mapper.fog_msg import fog_msg, DEFAULT_TILE_SIZE
from orpg.lib.xmlutil import parseXml

def fog_xml(string):
    xml_dom = parseXml(string)
    return xml_dom.childNodes[0]


def test_fog_msg_init():
    fog = fog_msg(None)
    assert not fog.enable
    assert fog.tile_size == DEFAULT_TILE_SIZE
    assert fog.children == {}

simple_fog = """
<fog enable='1'> 
  <tile id='12,34' bitmap='ABCD'/>
  <tile id='56,78' bitmap='EFGH'/>
</fog>
"""

def test_fog_msg_init_from_dom():
    fog = fog_msg(None)

    fog.init_from_dom(fog_xml(simple_fog))
    
    assert fog.enable
    assert len(fog.children) == 2
    assert fog.children["12,34"].get_prop("bitmap") == "ABCD"
    assert fog.children["56,78"].get_prop("bitmap") == "EFGH"

def test_fog_msg_set_from_dom_off():
    fog = fog_msg(None)

    fog.init_from_dom(fog_xml(simple_fog))

    assert fog.enable
    assert len(fog.children) > 0

    fog.set_from_dom(fog_xml("<fog enable='0'/>"))

    assert not fog.enable
    assert len(fog.children) == 0

def test_fog_msg_set_from_dom_size_change():
    fog = fog_msg(None)
    fog.init_from_dom(fog_xml(simple_fog))

    assert fog.enable
    assert len(fog.children) > 0

    fog.set_from_dom(fog_xml(f"<fog enable='1' tsize='{DEFAULT_TILE_SIZE // 2}'/>"))

    assert fog.enable
    assert fog.tile_size == DEFAULT_TILE_SIZE // 2
    assert len(fog.children) == 0
    assert fog.get_prop("enable") == "1"
    assert fog.get_prop("tsize") == f"{fog.tile_size}"

def test_fog_msg_set_from_dom_tile_change():
    fog = fog_msg(None)
    fog.init_from_dom(fog_xml(simple_fog))

    fog.set_from_dom(fog_xml(f"<fog enable='1'><tile id='12,34' bitmap='WXYZ'/></fog>"))

    assert fog.enable
    assert fog.tile_size == DEFAULT_TILE_SIZE
    assert len(fog.children) == 2
    assert fog.children["12,34"].get_prop("bitmap") == "WXYZ"

def test_fog_msg_set_from_dom_tile_add():
    fog = fog_msg(None)
    fog.init_from_dom(fog_xml(simple_fog))

    fog.set_from_dom(fog_xml(f"<fog enable='1'><tile id='54,32' bitmap='PQRS'/></fog>"))

    assert fog.enable
    assert fog.tile_size == DEFAULT_TILE_SIZE
    assert len(fog.children) == 3
    assert fog.children["54,32"].get_prop("bitmap") == "PQRS"

def test_fog_msg_set_from_dom_tile_del():
    fog = fog_msg(None)
    fog.init_from_dom(fog_xml(simple_fog))

    fog.set_from_dom(fog_xml(f"<fog enable='1'><tile id='12,34' bitmap=''/></fog>"))

    assert fog.enable
    assert fog.tile_size == DEFAULT_TILE_SIZE
    assert len(fog.children) == 1
    assert "12,34" not in fog.children
