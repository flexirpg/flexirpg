# FlexiRPG -- Tests for html_to_text()
#
# Copyright 2020 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from orpg.lib.text import html_to_text

def test_lib_text_html_to_text():
    assert html_to_text("<html><body>Text</body></html") == "Text"
    assert html_to_text("<dice result='67'>67</dice> --&gt;") == "67 -->"
