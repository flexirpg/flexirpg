"""mapper.map_utils unit tests."""

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

__copyright__ = "Copyright 2023 David Vrabel"

from collections import namedtuple

import pytest

from orpg.mapper.pathfinder import PathfinderPath, Square


Point = namedtuple("Point", ["x", "y"])

@pytest.mark.parametrize(
    "points,expected_squares,expected_distance",
    [
        ([Point(10, 20), Point(30, 20)],
         [Square(1, 2), Square(2, 2), Square(3, 2)],
         2),
        ([Point(10, 19), Point(10, 35)],
         [Square(1, 1), Square(1, 2), Square(1, 3)],
         2),
        ([Point(10, 10), Point(20, 20)],
         [Square(1, 1), Square(2, 2)],
         1),
        ([Point(10, 10), Point(20, 20), Point(30, 30)],
         [Square(1, 1), Square(2, 2), Square(3, 3)],
         3),
        ([Point(10, 10), Point(20, 20), Point(30, 30), Point(40, 40)],
         [Square(1, 1), Square(2, 2), Square(3, 3), Square(4, 4)],
         4),
    ])
def test_mapper_pathfinder_pathfinderpath(points, expected_squares, expected_distance):
    path = PathfinderPath(points, 10.0)
    assert path._squares == expected_squares
    assert path.distance == expected_distance
