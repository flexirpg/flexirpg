"""Unit tests for orpg.mapper.units."""

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

__copyright__ = "Copyright 2023 David Vrabel"

import pytest

from orpg.mapper.units import Units, MeasurementMethod

@pytest.mark.parametrize(
    "unit,expected_value,expected_str,expected_symbol",
    [
        (Units.METRES, 0, "metres", "m"),
        (Units.SQUARES, 1, "squares", "sq"),
        (Units.FEET, 2, "feet", "ft"),
    ])
def test_mapper_units_units(unit, expected_value, expected_str, expected_symbol):
    assert unit.value == expected_value
    assert str(unit) == expected_str
    assert unit.symbol == expected_symbol


@pytest.mark.parametrize(
    "method,expected_value,expected_str",
    [
        (MeasurementMethod.GEOMETRIC, 0, "Geometric"),
        (MeasurementMethod.PATHFINDER, 1, "Pathfinder"),
    ])
def test_mapper_units_measurementmethod(method, expected_value, expected_str):
    assert method.value == expected_value
    assert str(method) == expected_str
