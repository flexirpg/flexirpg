# mapper.displayimage tests.
#
# Copyright (C) 2021 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from orpg.lib.imageid import ImageId
import orpg.mapper.displayimage

import pytest
import wx

@pytest.mark.parametrize(
    "width,height",
    [
        (20, 20),
        (20, 40),
        (40, 20),
    ])
def test_mapper_displayimage_non_square(width, height):
    im = orpg.mapper.displayimage.DisplayImage(ImageId("1"), (width, height))

    # Basic check of generated image.
    assert im.wximage.IsOk()
    assert im.wximage.GetAlpha(0, 0) == 255
    assert im.wximage.GetAlpha(width//2, height//2) == 0
    assert im.wximage.GetAlpha(width-1, height-1) == 255
