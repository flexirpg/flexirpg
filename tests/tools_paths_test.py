# tools.paths tests.
#
# Copyright (C) 2020 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import os
import tempfile

from orpg.tools.paths import _Paths


def test_paths_image():
    paths = _Paths()
    assert paths.image("abc") == paths.image() + os.sep + "abc"

def test_paths_template():
    paths = _Paths()
    assert paths.template("abc") == paths.template() + os.sep + "abc"

def test_paths_user():
    paths = _Paths()
    assert paths.user() == os.environ["HOME"] + os.sep + ".flexirpg"
    assert paths.user("abc") == paths.user() + os.sep + "abc"

def test_paths_log():
    paths = _Paths()
    assert paths.log("abc") == paths.log() + os.sep + "abc"

def test_paths_config():
    paths = _Paths()
    # Override default user path to a temporary directory.
    with tempfile.TemporaryDirectory() as user_path:
        paths._paths["user"] = user_path
        assert paths.config("tree.xml") == user_path + "/tree.xml"
        assert os.path.exists(user_path + "/tree.xml")
