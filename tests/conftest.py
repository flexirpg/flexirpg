# pytest configuration and test fixtures.
#
# Copyright (C) 2019 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import tempfile

import pytest


@pytest.fixture
def config_path():
    with tempfile.NamedTemporaryFile() as f:
        yield f.name
