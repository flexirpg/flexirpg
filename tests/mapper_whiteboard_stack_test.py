# WhiteboardStack tests
#
# Copyright (C) 2019 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from orpg.mapper.whiteboard_object import WhiteboardObject
from orpg.mapper.whiteboard_stack import WhiteboardStack

class test_obj(WhiteboardObject):
    def __init__(self, num):
        self.num = num
        WhiteboardObject.__init__(self, None, "id-" + str(num))

def test_mapper_whiteboardstack():
    obj = {}
    stack = WhiteboardStack()
    for i in range(0, 11):
        obj[i] = test_obj(i)

    def check(result):
        assert len(stack) == len(result)
        for obj, num in zip(stack, result):
            assert obj.num == num

    for i in range(0, 10):
        stack.append(obj[i])
    check([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    stack.raise_(obj[9]) # last
    check([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    stack.lower(obj[0]) # first
    check([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    stack.raise_(obj[2])
    check([0, 1, 3, 2, 4, 5, 6, 7, 8, 9])

    stack.lower(obj[7])
    check([0, 1, 3, 2, 4, 5, 7, 6, 8, 9])

    stack.insert(obj[10], 4)
    check([0, 1, 3, 2, 10, 4, 5, 7, 6, 8, 9])

    stack.remove(obj[2])
    check([0, 1, 3, 10, 4, 5, 7, 6, 8, 9])

    stack.raise_to_top(obj[1])
    check([0, 3, 10, 4, 5, 7, 6, 8, 9, 1])

    stack.lower_to_bottom(obj[9])
    check([9, 0, 3, 10, 4, 5, 7, 6, 8, 1])

    stack.move(obj[0], 7)
    check([9, 3, 10, 4, 5, 7, 6, 0, 8, 1])

    for o in stack:
        stack.remove(o)
    check([])
